---
title: "flat_check_data.Rmd empty"
output: html_document
editor_options: 
  chunk_output_type: console
---

```{r development, include=FALSE}
library(testthat)
```

```{r development-load}
# Load already included functions if relevant
pkgload::load_all(export_all = FALSE)
```

# Get a message about the input color data : check_data

```{r function-check_data}
#' Get a message with check data.
#'
#' @param string Character. Data of Squirrels
#'
#'#' @return Side effect. Outputs a message in the console
#' @export
#'
#' @examples
check_primary_color_is_ok <- function(string) {
  
  all_colors_OK <- all(
    string %in% c("Gray", "Cinnamon", "Black", NA)
  )
  
    if (isFALSE((all_colors_OK))) {
    stop("Squirrel color does not exist")
  } else {
    message("Squirrel color exists")
  }
  
}
```

```{r examples-check_data}

check_primary_color_is_ok("Cinnamon")
# check_primary_color_is_ok("Red")

```

```{r tests-check_data}

test_that("check_primary_color_is_ok is a function", {
  expect_true(inherits(check_primary_color_is_ok, "function"))
})

test_that("get_message_fur_color works", {
  expect_message(
    object = check_primary_color_is_ok(string =  "Cinnamon"),
    regexp = "Squirrel color exists"
  )
})

test_that("get_message_fur_color works", {
  expect_message(
    object = check_primary_color_is_ok(string =  "Black"),
    regexp = "Squirrel color exists"
  )
})


```

# Get a message about the input color data : check_squirrel_data_integrity
    
```{r function-check_squirrel_data_integrity}
#' Get a message with check data.
#'
#' @param df_squirrels data.frame. Data of Squirrels
#'
#'#' @return Side effect. Outputs a message in the console
#' @export
#'
#' @examples
check_squirrel_data_integrity <- function(df_squirrels){
    check_name_exist <- "primary_fur_color" %in% names(df_squirrels)
    
    if(isFALSE(check_name_exist)) {
      stop("There is no column primary_fur_color")
    }
    
    primary_color_is_ok <- check_primary_color_is_ok(df_squirrels$primary_fur_color)
    
        if(isTRUE(primary_color_is_ok)) {
      stop("All primary fur color are ok")
    }
    
}
```
  
```{r example-check_squirrel_data_integrity}
# pkgload::load_all()

nyc_squirrels_sample <- readr::read_csv(system.file("nyc_squirrels_act_sample.csv", package = "squirreljuan"))

check_squirrel_data_integrity(nyc_squirrels_sample)

```
  
```{r tests-check_squirrel_data_integrity}
test_that("check_squirrel_data_integrity works", {
  expect_true(inherits(check_squirrel_data_integrity, "function")) 
})
```
  

```{r development-inflate, eval=FALSE}
# Run but keep eval=FALSE to avoid infinite loop
# Execute in the console directly

fusen::inflate(flat_file = "dev/flat_check_data.Rmd", 
               vignette_name = "Check data")

```




